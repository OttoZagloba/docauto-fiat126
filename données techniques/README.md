 Cette partie contient :

- des listes, catalogues de pièces, etc, livrés sans frais, dépourvus d'interdiction de reproduction ;
- des données techniques nécessaires à l'entretien ou la réparation du véhicule ;
