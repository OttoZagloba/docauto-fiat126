Voir https://fiat126maluch.wordpress.com/category/accessoires-electricite/
----------------------------------------------------------------------------
Liste des ampoules montées d’origine sur une 126 FL et l’option de remplacement en LED possible :

    **2 projecteurs longue portée**
    Capacité nominale [W] : 45/40
    Tension [V] : 12
    Type de culot : P45t
    Type de lampe : R2
    => remplacés par Car Rover® H4  Canbus LED 4000lm (40/30W)

    **2 feux de position/d’encombrement AV**
    Capacité nominale [W] : 4
    Tension [V] : 12
    Type de culot : BA 9 s
    Type de lampe : T4W
    => remplacés par OSRAM LED Premium Retrofit BA9s T4W 3850WW-02B (1W)
    
    ** 2 feux stop/position AR**
    Capacité nominale [W] : 21/5
    Tension [V] : 12
    Type de culot : BAY15d
    Type de lampe : P21/5W
    => remplacés par OSRAM LED Premium Retrofit P21/5W O-1557YE (2W/0,4W)
    
    **feu éclaireur de plaque AR**
    Capacité nominale [W] : 5
    Tension [V] : 12
    Type de culot : ?
    Type de lampe : W5W
    => remplacé par OSRAM LED Premium Retrofit W2.1×9.5d 2850WW-02B (1W)
    
    **feu anti-brouillard AR**
    Capacité nominale [W] : 21
    Tension [V] : 12
    Type de culot : BA15s
    Type de lampe : P21W
    => remplacé par  OSRAM LED Premium Retrofit BA15s P21W 7556WW-01B (4W)
    
    **feu de recul AR**
    Capacité nominale [W] : 21
    Tension [V] : 12
    Type de culot : BA15s
    Type de lampe : P21W
    => remplacé par  OSRAM LED Premium Retrofit BA15s P21W 7556WW-01B (4W)
    
    **plafonnier**
    Capacité nominale [W] : 5
    Tension [V] : 12
    Type de culot : SV8,5-8
    Type de lampe : C5W
    => remplacé par OSRAM LED Premium Retrofit SV8.5-8 36mm C5W 6498WW-01B (1W)

