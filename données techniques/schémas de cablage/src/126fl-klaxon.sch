EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:126fl-klaxon-cache
EELAYER 25 0
EELAYER END
$Descr User 6324 8291
encoding utf-8
Sheet 1 1
Title "Branchement du Klaxon de la 126p FL"
Date "2017-03-05"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Buzzer Buzzer6
U 1 1 58BC1544
P 1350 2050
F 0 "Buzzer6" H 1500 2100 50  0000 L CNN
F 1 "Klaxon6" H 1500 2000 50  0000 L CNN
F 2 "" V 1325 2150 50  0000 C CNN
F 3 "" V 1325 2150 50  0000 C CNN
	1    1350 2050
	-1   0    0    1   
$EndComp
$Comp
L Jumper_NO_Small Commande37
U 1 1 58BC2545
P 2700 2500
F 0 "Commande37" H 2700 2580 50  0000 C CNN
F 1 "Jumper_NO_Small" H 2710 2440 50  0000 C CNN
F 2 "" H 2700 2500 50  0000 C CNN
F 3 "" H 2700 2500 50  0000 C CNN
	1    2700 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2150 2400 2150
Wire Wire Line
	2400 2150 2600 2500
Text Label 2100 2150 0    60   ~ 0
f-violet
Wire Wire Line
	1450 1950 2450 1950
Wire Wire Line
	2450 1950 3250 900 
Text Label 2300 1950 0    60   ~ 0
sc-gris_noir
$Comp
L Fuse Fuse15
U 1 1 58BC27F5
P 1650 6150
F 0 "Fuse15" V 1730 6150 50  0000 C CNN
F 1 "Fusible15 A" V 1575 6150 50  0000 C CNN
F 2 "" V 1580 6150 50  0000 C CNN
F 3 "" H 1650 6150 50  0000 C CNN
	1    1650 6150
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 2500 1500 2500
Wire Wire Line
	1500 2500 1500 6150
Text Label 1500 3750 0    60   ~ 0
f-violet
$Comp
L Jumper_NO_Small Commande33
U 1 1 58BC3D52
P 5050 1850
F 0 "Commande33" H 5050 1930 50  0000 C CNN
F 1 "Jumper_NO_Small" H 5060 1790 50  0000 C CNN
F 2 "" H 5050 1850 50  0000 C CNN
F 3 "" H 5050 1850 50  0000 C CNN
	1    5050 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 900  5150 1400
Wire Wire Line
	5150 1400 5150 1850
Text Label 5150 1400 0    60   ~ 0
sc-gris_noir
$Comp
L CONN_02X04 Connecteur25
U 1 1 58BC79A9
P 3500 1050
F 0 "Connecteur25" H 3500 1300 50  0000 C CNN
F 1 "CONN_02X04" H 3500 800 50  0000 C CNN
F 2 "" H 3500 -150 50  0000 C CNN
F 3 "" H 3500 -150 50  0000 C CNN
	1    3500 1050
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X06 Connecteur27
U 1 1 58BC7C4F
P 4000 2350
F 0 "Connecteur27" H 4000 2700 50  0000 C CNN
F 1 "CONN_02X06" H 4000 2000 50  0000 C CNN
F 2 "" H 4000 1150 50  0000 C CNN
F 3 "" H 4000 1150 50  0000 C CNN
	1    4000 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 2500 4950 2000
Wire Wire Line
	4950 2000 4950 1850
$Comp
L CONN_02X04 Connecteur28
U 1 1 58BCAA4A
P 4150 4100
F 0 "Connecteur28" H 4150 4350 50  0000 C CNN
F 1 "CONN_02X04" H 4150 3850 50  0000 C CNN
F 2 "" H 4150 2900 50  0000 C CNN
F 3 "" H 4150 2900 50  0000 C CNN
	1    4150 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 6150 1800 6150
Wire Wire Line
	3900 4050 3300 6150
$Comp
L +12V #PWR01
U 1 1 58BCB055
P 2750 3750
F 0 "#PWR01" H 2750 3600 50  0001 C CNN
F 1 "+12V" H 2750 3890 50  0000 C CNN
F 2 "" H 2750 3750 50  0000 C CNN
F 3 "" H 2750 3750 50  0000 C CNN
	1    2750 3750
	1    0    0    -1  
$EndComp
Text Label 3050 6150 0    60   ~ 0
o-brun
Text Label 3300 3750 0    60   ~ 0
o-brun
Connection ~ 3900 4050
$Comp
L GND #PWR02
U 1 1 58BCB3F8
P 2650 3100
F 0 "#PWR02" H 2650 2850 50  0001 C CNN
F 1 "GND" H 2650 2950 50  0000 C CNN
F 2 "" H 2650 3100 50  0000 C CNN
F 3 "" H 2650 3100 50  0000 C CNN
	1    2650 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2500 2650 3100
Text Label 3000 2900 0    60   ~ 0
c-noir:bc-blanc_noir
Wire Wire Line
	2750 3750 3850 3750
Wire Wire Line
	3850 3750 3900 4050
Text Notes 4600 1700 0    60   ~ 0
Commande du klaxon\n
$EndSCHEMATC
