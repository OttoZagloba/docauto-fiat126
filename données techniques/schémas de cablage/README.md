*schemat instalacji elektrycznej* 126p **FL**
=============================================

![1](126fl-schemat instalacji elektrycznej-1992.png)

## fils

(b - biały, blanc ; c - czarny, noir ; f - fioletowy, violet ; k -
czerwony, rouge ; l - błękitny, azur ; n - niebieski, bleu ; o -
brązowy, brun ; p - pomarańczowy, orange ; r - różowy, rose ; s -
szary, gris ; t - zielony, vert ; z - żółty, jaune)

**Correctif :**
* couleur de 40 **bc-46** à 56 **bo-40** => mis en **bo**
* couleur de 42 **zo-22** à 22 **zc-42** => mis en **zc**
* couleur de 42 **zc-46** à 56 **zo-42** => mis en **zo**
* jonction de 51 **o-57**  à 67 **o-51** => 51 **o-67**

## éléments

### alimentation / masse *rouge*

* 3, 10, 16, 52. masse
* 4. batterie
* 5. connecteur de batterie
* 13. porte-fusible de dégivrage lunette arrière
* 15. boîtier de fusibles
* 25, 26, 27. connecteurs *sous la direction* (złącza przełącznika pod
  kierownicą) effectifs sur 30, 31, 32 et 33
* 40, 41, 42, 43. connecteurs du faisceau de liaison avant-arrière
* 48, 55. fil résistif
* 54, 63, 66, 69. condensateur
* 64. alternateur
* 67. régulateur de voltage/de l'alternateur


### consommateurs essentiels *violet*

* 51. démarreur
* 53. allumeur
* 60. bobine d'allumage
* 65. bougies
* 70. électrovanne étouffeuse de ralenti

### consommateurs secondaires *orange*

* 6. klaxon
* 11. pompe de projection de liquide lave-glace
* 17. moteur d'essuie-glace
* 20. turbine de ventilation de chauffage
* 22. connecteur d'allume-cigare
* 23. connecteur d'autoradio
* 49. dégivrage lunette arrière

### capteurs, interrupteurs automatiques *cyan*

* 12. capteur de niveau de liquide de frein
* 18. relai des clignotants de détresse
* 19. relai des essuie-glace
* 21. relai des clignotants de direction
* 47. sonde du niveau d'essence dans le réservoir
* 50. commutateur de feu de recul
* 58. capteur de pression d'huile

### commandes, interrupteurs manuels *bleu*

* 7. commutateur de feu stop : pédale
* 28. connecteur du commutateur d'allumage
* 29. commutateur d'allumage
* 30. levier des essuie-glace et de pompe de projection de lave-glace
* 31. levier des feux de croisement et de route
* 32. levier des clignotants de changement de direction
* 33. interrupteur du klaxon
* 34. interrupteur de la turbine de ventilation 
* 35. interrupteur du feu antibrouillard AR
* 37. interrupteur des clignotants de détresse
* 38. interrupteur du dégivrage lunette arrière
* 39. interrupteur des feux de position
* 45. interrupteur du plafonnier par ouverture de la porte conducteur 
* 46. commutateur de feu stop : frein à main


### feux, voyants et indicateurs *jaune*

* 1. phare (route, croisement, position) avant droit
* 2. clignotant de direction avant droit
* 8. phare (route, croisement, position) avant gauche
* 9. clignotant de direction avant gauche
* 14. (répétiteur) clignotant de direction latéral gauche
* 24. (répétiteur) clignotant de direction latéral droit
* 36. tableau de bord
  - l. éclairage du tableau de bord
  - b. témoin frein à main/carence de liquide de frein
  - c. témoin essence sur la réserve
  - d. témoin des clignotants de direction
  - e. témoin du feu antibrouillard arrière
  - f. témoin du dégivrage lunette arrière
  - i. témoin de pression d'huile moteur insuffisante
  - j. témoin batterie
  - h. témoin des clignotants de détresse
  - g. témoin des feux de route
  - k. témoin des feux de position
  - a. jauge d'essence dans le réservoir
* 44. plafonnier
* 56. feu principal arrière droit
* 57. feu de recul
* 59. éclairage de la plaque d'immatriculation arrière
* 61. feu antibrouillard arrière
* 62. feu principal arrière gauche


![1](126fl-schemat instalacji elektrycznej-1992-legende.png)

## mémento

* czujnik = capteur
* gniazdo = douille
* przełącznik/przerywacz = interrupteur/relai
* wskaźnik = indicateur (ex : jauge d'essence)
* wyłącznik = commutateur
* złącze = connecteur



*bezpieczniki topikowe instalacji elektrycznej* 126p **FL**
============================================================

Fusibles 8A « torpedo », A étant celui le plus
vert l'avant du véhicule, H celui le plus proche de l'habitacle :

* A. plafonnier, klaxon, clignotant de détresse et leur témoin,
  éventuellement autoradio ;
* B. Clignotants de changement de direction et leur témoin,
  essuie-glace, pompe de projection de liquide lave-glace, témoin
  essence sur la réserve et jauge d'essence dans le réservoir, témoin
  frein à main/carence de liquide de frein, témoin de pression d'huile
  moteur insuffisante, feu de stop, feu de recul, turbine de
  ventilation electrique de chauffage ;
* C. feu de route gauche et son témoin ;
* D. feu de route droit ; 
* E. feu de croisement gauche ;
* F. feu de croisement droit, anti-brouillard arrière et son témoin ;
* G. feux de position avant gauche et arrière droit, éclairage de la plaque d'immatriculation arrière ;
* H. feux de position avant droit et arrière gauche, témoin feux de positions, éclairage du tableau de bord.

Note : le fusible de dégivrage de lunette arrière est sur un porte
fusible à part (cf. 13 sur le schéma).

![1](126fl-bezpieczniki - naklejka.png)
![1](126fl-bezpieczniki.png)

