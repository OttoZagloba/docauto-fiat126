Documentation portant sur la fiat 126 soit libre soit usage loyal (https://fr.wikipedia.org/wiki/Fair_use), notamment :

- dont la publication a cessé ;
- commentée sur un aspect spécifique ;
- livrée sans frais avec le véhicule et ne comportant aucune mention restrictive d'usage ;
- cataloguant des pièces et indiquant des références ;
    
L'objet principal de celle-ci est de contribuer à satisfaire aux obligations légales de l'article 311-1 du code de la route : « Les véhicules doivent être construits, commercialisés, exploités, utilisés, entretenus et, le cas échéant, réparés de façon à assurer la sécurité de tous les usagers de la route ».

**Voir aussi le blog https://fiat126maluch.wordpress.com/ dont :**

- [autoradio : montage sans découpe ni trou percé](https://fiat126maluch.wordpress.com/2015/03/09/montage-un-autoradio-sans-decoupe-ni-trou-perce/) ;
- [boîte de vitesse : changement du disque d'embrayage, avec dépose du moteur ](https://fiat126maluch.wordpress.com/2016/01/20/changement-du-disque-dembrayage/) ;
- [boîte de vitesse : vidange](https://fiat126maluch.wordpress.com/2016/07/23/vidange-de-la-boite-de-vitesse/) ; 
- [boîte de vitesse : difficultés à passer des vitesses liées à la partie commande](https://fiat126maluch.wordpress.com/2017/06/05/difficultes-a-passer-des-vitesses-sur-une-fiat-126-liees-a-la-partie-commande/) ; 
- [carburateur : spécificités des 126p](https://fiat126maluch.wordpress.com/2017/03/01/specificites-des-carburateurs-montes-sur-les-126p/) ;
- [démarreur : changement des balais de charbon](https://fiat126maluch.wordpress.com/2017/03/27/changement-des-balais-de-charbon-du-demarreur/) ;
- [éclairage : passage aux LED](https://fiat126maluch.wordpress.com/2016/10/02/remplacement-de-leclairage-a-filament-en-led/) ;
- [freins : montage de freins à disques, avec dépose de la fusée](https://fiat126maluch.wordpress.com/2017/02/18/montage-de-freins-a-disque-a-lavant/) ; 
- [fusibles : autocollant pour boîte porte-fusibles](https://fiat126maluch.wordpress.com/2017/02/18/montage-de-freins-a-disque-a-lavant/) ; 
- [habitacle : restauration des garnitures de porte](https://fiat126maluch.wordpress.com/2015/03/12/restauration-garniture-de-porte/) ;
- [train roulant : changement des amortisseurs avant et arrière"](https://fiat126maluch.wordpress.com/2017/06/25/changement-des-amortisseurs-avant-et-arriere-de-fiat-126p/) ;
- [train roulant : passage de 12" à 13"](https://fiat126maluch.wordpress.com/2016/01/24/changement-de-jantes-et-pneus-de-12-vers-13/) ;

![1](ottoz.png)
