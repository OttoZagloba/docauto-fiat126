Voir https://fiat126maluch.wordpress.com/2017/02/17/achat-de-pieces-de-126-en-pologne-traduction-du-nom-des-pieces/
----------------------------------------------

# Liste détaillée dans [pièces po polsku](pièces po polsku.txt).

# En images :

## *moteur*
![1](img-silnik1.jpg)
![1](img-silnik2.jpg)
![1](img-silnik3.jpg)
![1](img-silnik4.jpg)
![1](img-silnik5.jpg)
![1](img-silnik6.jpg)

## *suspension/train roulant*
![1](img-podwozie1.jpg)
![1](img-podwozie2.jpg)
![1](img-podwozie3.jpg)

## *freinage*
![1](img-hamulce.jpg)

## *échappement*
![1](img-wydech.jpg)

## *boîte de vitesses*
![1](img-skrzynia biegow.jpg)

## *carrosserie*
![1](img-karoseria1.jpg)
![1](img-karoseria2.jpg)





