#!/bin/bash

echo "Handling files starting by string UMYYYY"
for img in UM*.jpg; do
    if [ ! -e "$img" ]; then break ; fi
    
    finalimg=`echo "$img" | cut -c 3-`
    year=`echo $finalimg | cut -c 1-4`
    echo "$year -> $finalimg"
    width=`identify -format %w "$img"`; \
    convert -background '#0008' -fill white -gravity center -size ${width}x30 \
          caption:"$year" \
          "$img" +swap -gravity south -composite "$finalimg"
    rm -f "$img"
done

echo "Handling files starting by string UTtarifYYYY-NNNNzł"
for img in UT*.jpg; do
    if [ ! -e "$img" ]; then break ; fi
    finalimg=`echo "$img" | cut -c 3-`
    year=`echo $finalimg | cut -c 6-9`
    prixzl=`echo $finalimg | cut -c 11- | sed s/zł\.jpg$//g`
    prixeur=`echo "$prixzl * .23814" | bc -l | awk '{print int($1)}'`
    echo "$year $prixzl zł / $prixeur € -> $finalimg"
    width=`identify -format %w "$img"`; \
    convert -background '#0008' -fill white -gravity center -size ${width}x30 \
          caption:"$year à $prixzl zł/$prixeur € (en `date "+%B %Y"`)" \
          "$img" +swap -gravity south -composite "$finalimg"
    rm -f "$img"
done
